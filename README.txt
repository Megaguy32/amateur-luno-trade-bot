Amateur Luno Trade Bot
- - -

A python program that buys and sells between bitcoin (XBT) and SA Rand (ZAR), and tries to
accumulate Bitcoin in the supplied wallet

The theoretical script doesn't actually buy or sell, but runs the logic as if
the markie.txt file is the market price.
Practical functionality is not yet implemented (but there are relevant
comments in the code).

Check the Luno API,  https://www.luno.com/en/developers


Quick Start:
$  pip install luno-python
$  ./luno-project-theoretical.py


A word of warning. Verify if the code is safe for yourself.
I'm not responsible.

This is a bigtime experiment and WIP.


Configuration currently takes the suckless (for better or for worse)
approach, modify the constants in the code.