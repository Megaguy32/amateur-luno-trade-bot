#!/usr/bin/python3

from datetime import datetime as dt
from luno_python.client import Client
from time import sleep
import sys
import os
import json


# Constants
KEY = ""
SECRET = ""
MAX_UNFINISHED = 5  # total number of unfinished trade pairs allowed
ZAR_SPEND_AMOUNT = 300  # zar amount to spend on bitcoin
#   CAUTION: spend amount / market price should be above 0.0005,
#   thats the minimum bitcoin sell amount (enforced by Luno API, not by this code)
PREFERRED_INCREASE: float = 0.004  # used to calculate price to sell at (see calculate_sell_at)
#       Note, its in decimal and not percentage
#       current ZA fees are 0.1% (thus 0.001), therefore anything above that
#       is considered a profit. this is not enforced in code, so set variable
#       higher than that by yourself
INTERVAL = 10  # Time between market checks. In seconds.
FILEPATH_PREFIX = "theoretical/"  # either "theoretical/" or "practical/" (note the slash)
MARKET_CHECK_API = False
#   True: check market using the Luno API,
#   False: check fake market using the text file
BUY_MODE = True  # Set to false to not buy,

# Singletons
the_market = {"current_price": None}
client = Client(api_key_id=KEY, api_key_secret=SECRET)


def create_transaction(tx_type, btc_sell_amount=None, zar_spend_amount=None, bought_at=None, sold_at=None):

    # amount of btc to sell.  → base_volume in luno API
    # amount of zar to earn
    # amount of zar to spend. → counter_volume in luno API
    # amount of btc to buy.

    tx = {
        "type": tx_type,
        "pair": "XBTZAR",
        "bought_at": bought_at,
        "zar_spend_amount": zar_spend_amount,
        "sold_at": sold_at,
        "btc_sell_amount": btc_sell_amount,
    }
    # TODO: Consider adding a timestamp, perhaps just for organizational purposes.
    return tx


def ping():
    check_the_market()
    create_unfinished_transaction()
    check_for_unfinished_transactions("unfinished")
    check_for_unfinished_transactions("archived")


def check_for_unfinished_transactions(foldername):
    unfinished_files = os.listdir(FILEPATH_PREFIX + foldername)
    
    # Try to finish unfinished pairs
    for filename in unfinished_files:
        if filename.endswith(".txt"):
            filepath = FILEPATH_PREFIX + foldername + "/" + filename
            the_file = open(filepath, "r")
            tx = json.load(the_file)
            check_for_sell(tx, filepath)

def create_unfinished_transaction():
    unfinished_files = os.listdir(FILEPATH_PREFIX + "unfinished")
    if len(unfinished_files) < MAX_UNFINISHED:
        if not BUY_MODE:
            return
        # TODO if i'm not broke:
        buy(ZAR_SPEND_AMOUNT)


def check_for_sell(tx_buy, filepath):
    """Takes an unfinished transaction and checks if a sell should be made to
    complete it"""
    
    print("Checking if able to make profit regarding this transaction:\n\t", tx_buy)
    
    current_price = the_market["current_price"]
    
    sell_at = calculate_sell_at(tx_buy)  # Market price to sell at. not the amount
    
    print("Sell at:", sell_at)
    
    # Check for sell
    if current_price >= sell_at:
        print("!!! SELL !!!")
        # TODO there's a chance the ZAR_SPEND_AMOUNT is changed between runs,
        #   btc_sell_amount should be calculated usung the buy_transaction's
        #   "amount of zar spent" variable
        btc_sell_amount = tx_buy["zar_spend_amount"] / current_price
        btc_sell_amount = round(btc_sell_amount, 6)
        sell(btc_sell_amount, filepath)
    else:
        print("hmm, not this time it seems...")
    print()


def check_the_market():
    print("\nPing! time to check the market")
    print("- - -")
    
    res = None
    current_price = None
    
    try:

        if MARKET_CHECK_API:
            # Luno API
            res = client.get_ticker(pair="XBTZAR")
            #print(res)
            current_price = float(res["ask"])
        else:
            # Text file
            the_file = open("markie.txt", "r")
            the_text = the_file.read()
            current_price = float(the_text)

    except Exception as e:
        slimey(e)

    print("Market check successful")
    the_market["current_price"] = current_price
    print("Market price: " + str(the_market["current_price"]))
    print("\n")
    

def sell(btc_sell_amount, old_filepath):
    if FILEPATH_PREFIX == "theoretical/":
        print("Theoretically Selling " + str(btc_sell_amount) + " BTC")
    else:
        print("Actually Selling " + str(btc_sell_amount) + " BTC")
    # TODO:
    #       keep in mind, posting an order doesn't mean another person bought it,
    #       Consider implementing a system where the transaction pair only
    #       finishes once the btc sale is a success.
    #       That however increases the complexity of the program.

    sold_at = the_market["current_price"]
    sell_transaction = create_transaction("SELL", btc_sell_amount=btc_sell_amount, sold_at=sold_at)

    if FILEPATH_PREFIX == "practical/":
        try:
            p = sell_transaction["pair"]
            t = sell_transaction["type"]
            c = sell_transaction["zar_spend_amount"]
            sellres = client.post_market_order(pair="XBTZAR", type="SELL", base_volume=btc_sell_amount)
            #print(sellres)

            pass
        except Exception as e:
            slimey(e)
        
    print("sell was a success, continuing...")
    print("creating a purchase file")

    read_file = open(old_filepath, "r")
    buy_transaction = json.load(read_file)
    transaction_pair = {"buy": buy_transaction, "sell": sell_transaction}

    # Create new finished transaction pair file
    os.remove(old_filepath)
    date_string = dt.now().strftime("%Y%m%d-%H%M%S")
    # Prevent same filename problem
    file_counter = 0
    while True:
        new_filepath = FILEPATH_PREFIX + "finished/" + date_string + "-" + str(file_counter) + ".txt"
        if not os.path.isfile(new_filepath):
            break
        file_counter += 1
    the_file = open(new_filepath, "x")
    json.dump(transaction_pair, the_file)


def buy(zar_spend_amount):
    if FILEPATH_PREFIX == "theoretical/":
        print("Theoretically Buying R" + str(zar_spend_amount) + " worth of BTC")
    else:
        print("Actually Buying R" + str(zar_spend_amount) + " worth of BTC")
    
    bought_at = the_market["current_price"]
    buy_transaction = create_transaction("BUY", zar_spend_amount=zar_spend_amount, bought_at=bought_at)

    if FILEPATH_PREFIX == "practical/":
        try:
            p = buy_transaction["pair"]
            t = buy_transaction["type"]
            c = buy_transaction["zar_spend_amount"]
            buyres = client.post_market_order(pair=p, type=t, counter_volume=c)
            #print(buyres)

            pass
        except Exception as e:
            slimey(e)
        
    print("purchase was a success, continuing...")

    # Create a transaction file under the unfinished folder
    print("creating a purchase file")
    date_string = dt.now().strftime("%Y%m%d-%H%M%S")
    # Prevent same filename problem
    file_counter = 0
    while True:
        filepath = FILEPATH_PREFIX + "unfinished/" + date_string + "-" + str(file_counter) + ".txt"
        if not os.path.isfile(filepath):
            break
        file_counter += 1
    the_file = open(filepath, "x", encoding="utf-8")
    # Append buy_transaction details to file
    json.dump(buy_transaction, the_file)
    print("file created", str(the_file), "\n\n")
    

def slimey(e):
    print(e)
    print("\nYO YO YO my name is Slimey D. and an error has been encountered, so I'm exiting boi!\npeace✌️")
    sys.exit()


# TODO sell at, consider the commented feature proposals
def calculate_sell_at(tx_buy):
    """ use
        - bought at
        - preferred % increase profit (in zar or in btc, is it the same?)
        - or zar profit
        - take fees into consideration (as a constant, or a dynamic look up)
        
        i_bought_at = current_price
        sell_at = i_bought_at * (1 + wanted_percentage_increase)
        """
        
    bought_at = tx_buy["bought_at"]
    sell_at: float
    sell_at = bought_at * (1 + PREFERRED_INCREASE)
    sell_at = round(sell_at, 6)
    
    return sell_at


# Executed code
# - - -


# TODO: Reminder for future functionality. command line arguements.
#print("Arguments:", sys.argv)


while True:
    ping()
    sleep(INTERVAL)
